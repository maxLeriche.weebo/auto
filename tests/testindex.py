import unittest
from auto.index import *

class TestIndexFunctions(unittest.TestCase):
    
    def TestGetFieldName(self, test_tab):
        test_tab[0] = "address"
        test_tab[1] = "name"
        test_tab[2] = "firstname"
        test_tab[3] = "immat"
        test_tab[4] = "date_immat"
        test_tab[5] = "VIN"
        test_tab[6] = "marque"
        test_tab[7] = "denomination"
        test_tab[8] = "couleur"
        test_tab[9] = "carrosserie"
        test_tab[10] = "categorie"
        test_tab[11] = "cylindree"
        test_tab[12] = "energie"
        test_tab[13] = "places"
        test_tab[14] = "poids"
        test_tab[15] = "puissance"

        temp = test_tab[0]

        self.assertEqual(getFieldname(test_tab), test_tab)
        self.assertEqual(temp, test_tab[0])

    def TestTransfoDico(self, dict_to_test):
        dict_to_test[0] = "address"
        dict_to_test[1] = "name"
        dict_to_test[2] = "firstname"
        dict_to_test[3] = "immat"
        dict_to_test[4] = "date_immat"
        dict_to_test[5] = "VIN"
        dict_to_test[6] = "marque"
        dict_to_test[7] = "denomination"
        dict_to_test[8] = "couleur"
        dict_to_test[9] = "carrosserie"
        dict_to_test[10] = "categorie"
        dict_to_test[11] = "cylindree"
        dict_to_test[12] = "energie"
        dict_to_test[13] = "places"
        dict_to_test[14] = "poids"
        dict_to_test[15] = "puissance"

        temp = dict_to_test[0]

        self.assertEqual(transfodico(), dict_to_test)
        self.assertEqual(temp, dict_to_test[0])
