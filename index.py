import csv
from copy import copy
            
def getFieldname(value1):
    value1[0] = "address_titulaire"
    value1[1]="nom"
    value1[2]="prenom"
    value1[3]="immatriculation"
    value1[4]="date_immatriculation"
    value1[5]="vin"
    value1[6]="marque"
    value1[7]="denomination_commerciale"
    value1[8]="couleur"
    value1[9]="carrosserie"
    value1[10]="categorie"
    value1[11]="cylindree"
    value1[12]="energie"
    value1[13]="places"
    value1[14]="poids"
    value1[15]="puissance"
    value1.append("variante")
    value1.append("version")
    value1[16]="type"
    return value1

def transfodico():
    typed ,varianted,versiond = row['type_variante_version'].split(",")
    dictiotowrite = {
            'address_titulaire': row['address'],
            'nom':row['name'],
            'prenom':row['firstname'],
            'immatriculation':row['immat'],
            'date_immatriculation':row['date_immat'] ,
            'vin':row['vin'] ,
            'marque':row['marque'] ,
            'denomination_commerciale':row['denomination'] ,
            'couleur':row['couleur'] ,
            'carrosserie':row['carrosserie'] ,
            'categorie':row['categorie'] ,
            'cylindree':row['cylindree'] ,
            'energie':row['energy'] ,
            'places':row['places'] ,
            'poids':row['poids'] ,
            'puissance':row['puissance'] ,
            'type':typed ,
            'variante':varianted ,
            'version':versiond 
        }
    return dictiotowrite


def Ecriture(tab,writer):
        writer.writerow(tab)


with open('dep.csv') as csvfile: # dep pour depart au cas où# 
    
    reader = csv.DictReader(csvfile,delimiter='|')
    fieldnames=reader.fieldnames
    test=getFieldname(copy(fieldnames))
    with open('test.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter=';',fieldnames=test)
        writer.writeheader()
        for row in reader:
            Ecriture(transfodico(),writer)
        